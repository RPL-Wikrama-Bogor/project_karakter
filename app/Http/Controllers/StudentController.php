<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use App\Models\Rombel;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use stdClass;

class StudentController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $students = Student::latest()->paginate(10);

        // return view('students.index', [
        //     'students' => $students
        // ]);
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $students = Student::with('jurusan', 'rombel')->where('nomor_induk', 'LIKE', "%{$search}%")->paginate(10);
        }else {
            $students = Student::with('jurusan', 'rombel')->latest()->paginate(10);
        }

        return view('students.index', [
            'students' => $students,
        ])
        ->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jurusan = Jurusan::all();
        $rombel = Rombel::all();
        return view('students.create', compact('jurusan', 'rombel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nomor_induk' => 'required|max:255',
            'nama' => 'required|max:50',
            'jurusan_id' => 'required|max:255',
            'tahun_pelajaran' => 'required|max:255',
            'rombel_id' => 'required|max:255',
            'semester' => 'required|max:20'
        ]);

        Student::create($request->all());

        return redirect()->route('siswa.index')
                        ->with('success','Data berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jurusan = Jurusan::all();
        $rombel = Rombel::all();
        $student = Student::with('jurusan', 'rombel')->findOrFail($id);
        return view('students.edit', compact('student', 'rombel', 'jurusan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nomor_induk' => 'required|max:255',
            'nama' => 'required|max:50',
            'tahun_pelajaran' => 'required|max:255',
            'semester' => 'required|max:20'
        ]);

        $student = Student::findOrFail($id);
        $student->update($request->all());

        return redirect()->route('siswa.index')
                        ->with('success','Data berhasil diperbarui!');
    }

   /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::findOrFail($id);

        $student->delete();

        return redirect()->route('siswa.index');
    }
}
