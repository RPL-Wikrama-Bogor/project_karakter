<?php

namespace App\Http\Controllers;

use App\Models\CategoryCharacter;
use App\Models\Character;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class CharacterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('student')->where('siswa_id', 'LIKE', "%{$search}%")->paginate(10);
        }else {
            $characters = Character::with('student')->latest()->paginate(10);
        }

        return view('characters.index', [
            'characters' => $characters
        ])
        ->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::all();
        return view('characters.create', compact('students'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'karakter' => 'required',
            'siswa_id' => 'required',
            'capaian' => 'required|numeric',
            'deskripsi' => 'required',
        ]);

        Character::create($request->all());

        return redirect()->route('karakter.index')->with('success', 'Karakter berhasil dibuat!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $students = Student::all();
        $character = Character::findOrFail($id);

        return view('characters.edit', compact('character', 'students'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'karakter' => 'required',
            'siswa_id' => 'required',
            'capaian' => 'required|numeric',
            'deskripsi' => 'required',
        ]);

        $character = Character::findOrFail($id);
        $character->update($request->all());

        return redirect()->route('karakter.index')->with('success', 'Karakter berhasil diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $character = Character::findOrFail($id);
        $character->delete();

        return redirect()->route('karakter.index')->with('success', 'Karakter berhasil dihapus!');
    }
}
