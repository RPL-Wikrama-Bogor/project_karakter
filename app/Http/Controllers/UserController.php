<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Image;
class UserController extends Controller
{
    public function profile()
    {
        return view('profile', [
            'user' => Auth::user()
        ]);
    }

    public function update_profile(Request $request)
    {
       if($request->hasFile('avatar'))
       {
           $avatar = $request->file('avatar');
           $filename = time() . '.' . $avatar->getClientOriginalExtension();
           Image::make($avatar)->save(public_path('/uploads/avatars/' . $filename));

            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
       }

        /* Store $imageName name in DATABASE from HERE */

        return back()
            ->with([
                'user' => Auth::user()
            ])
            ->with('success','You have successfully update avatar.');
    }

    public function change_password(Request $request)
    {
        if(!(Hash::check($request->get('current_password'), Auth::user()->password)))
        {
            return back()->with('error', 'Your current password does not match with what you provided');
        }

        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0)
        {
            return back()->with('error', 'Your current password cannot be same with the new password');
        }

        $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:8|confirmed'
        ]);

        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return back()->with('success', 'Your password successfully changed');
    }
}
