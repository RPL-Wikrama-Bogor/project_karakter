@extends('layouts.admin.default')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">Account Settings</h2>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index-2.html">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="#">Pages</a>
                  </li>
                  <li class="breadcrumb-item active"> Account Settings
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
          <div class="form-group breadcrumb-right">
            <div class="dropdown">
              <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
              <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
            </div>
          </div>
        </div>
      </div>
      <div class="content-body"><!-- account setting page -->
<section id="page-account-settings">
<div class="row">
  <!-- left menu section -->
  <div class="col-md-3 mb-2 mb-md-0">
    <ul class="nav nav-pills flex-column nav-left">
      <!-- general -->
      <li class="nav-item">
        <a
          class="nav-link active"
          id="account-pill-general"
          data-toggle="pill"
          href="{{ route('profile') }}"
          aria-expanded="true"
        >
          <i data-feather="user" class="font-medium-3 mr-1"></i>
          <span class="font-weight-bold">General</span>
        </a>
      </li>
      <!-- change password -->
      <li class="nav-item">
        <a
          class="nav-link"
          id="account-pill-password"
          data-toggle="pill"
          href="{{ route('password') }}"
          aria-expanded="false"
        >
          <i data-feather="lock" class="font-medium-3 mr-1"></i>
          <span class="font-weight-bold">Change Password</span>
        </a>
      </li>
    </ul>
  </div>
  <!--/ left menu section -->

  <!-- right content section -->
  <div class="col-md-9">
    <div class="card">
      <div class="card-body">
        <div class="tab-content">
          <!-- general tab -->
          <div
            role="tabpanel"
            class="tab-pane active"
            id="account-vertical-general"
            aria-labelledby="account-pill-general"
            aria-expanded="true"
          >
          <!-- change password -->
          <div
            class="tab-pane fade"
            id="account-vertical-password"
            role="tabpanel"
            aria-labelledby="account-pill-password"
            aria-expanded="false"
          >
            <!-- form -->
            <form class="validate-form">
              <div class="row">
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label for="account-old-password">Old Password</label>
                    <div class="input-group form-password-toggle input-group-merge">
                      <input
                        type="password"
                        class="form-control"
                        id="account-old-password"
                        name="password"
                        placeholder="Old Password"
                      />
                      <div class="input-group-append">
                        <div class="input-group-text cursor-pointer">
                          <i data-feather="eye"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label for="account-new-password">New Password</label>
                    <div class="input-group form-password-toggle input-group-merge">
                      <input
                        type="password"
                        id="account-new-password"
                        name="new-password"
                        class="form-control"
                        placeholder="New Password"
                      />
                      <div class="input-group-append">
                        <div class="input-group-text cursor-pointer">
                          <i data-feather="eye"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label for="account-retype-new-password">Retype New Password</label>
                    <div class="input-group form-password-toggle input-group-merge">
                      <input
                        type="password"
                        class="form-control"
                        id="account-retype-new-password"
                        name="confirm-new-password"
                        placeholder="New Password"
                      />
                      <div class="input-group-append">
                        <div class="input-group-text cursor-pointer"><i data-feather="eye"></i></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-12">
                  <button type="submit" class="btn btn-primary mr-1 mt-1">Save changes</button>
                  <button type="reset" class="btn btn-outline-secondary mt-1">Cancel</button>
                </div>
              </div>
            </form>
            <!--/ form -->
          </div>
          <!--/ change password -->
        </div>
      </div>
    </div>
  </div>
  <!--/ right content section -->
</div>
</section>
<!-- / account setting page -->

      </div>
    </div>
  </div>
@endsection
