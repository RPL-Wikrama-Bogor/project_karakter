@extends('layouts.admin.default')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">Siswa Tables</h2>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('welcome') }}">Home</a>
                  </li>
                  <li class="breadcrumb-item active">Table Siswa
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
          <div class="form-group breadcrumb-right">
          </div>
        </div>
      </div>


<!-- Table Hover Animation start -->
<div class="row" id="table-hover-animation">
<div class="col-12">
  <div class="card">
    <div class="card-header">
      <h4 class="card-title">List Siswa</h4>

      <a href="{{ route('siswa.create') }}" class="btn btn-success">+ Tambah</a>
    </div>
    <form class="d-flex" action="{{ route('siswa.index') }}" method="GET">
        <div class="col-2"><div><input type="text" class="form-control" name="search" value="{{ request()->query('search') }}" placeholder="Cari nomor induk"></div></div>
        <button class="btn btn-primary"><i data-feather="search"></i></button>
    </form>
    <div class="card-body">
    </div>
    <div class="table-responsive">
      <table class="table table-hover-animation">
        <thead>
          <tr>
            <th>NO. Induk</th>
            <th>Nama</th>
            <th>Kompetensi</th>
            <th>Tahun Pelajaran</th>
            <th>Rombel</th>
            <th>Semester</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($students as $student)
            <tr>
                <td>{{ $student->nomor_induk }}</td>
                <td>{{ $student->nama }}</td>
                <td>{{ $student->jurusan->jurusan }}</td>
                <td>{{ $student->tahun_pelajaran }}</td>
                <td>{{ $student->rombel->rombel }}</td>
                <td>{{ $student->semester }}</td>
                <td>
                        <form action="{{ route('siswa.destroy',$student->id) }}" method="POST">
                                  <a class="btn btn-warning" href="{{ route('siswa.edit',$student->id) }}">
                                    <i data-feather="edit-2" class="mr-50"></i>
                                    <span>Edit</span>
                                  </a>

                                  @csrf
                                  @method('DELETE')
                                  <button class="btn btn-danger"><i data-feather="trash" class="mr-50"></i><span>Delete</span></button>
                        </form>
                </td>
              </tr>
            @empty
            <tr>
                <td colspan="7" class="text-center">
                    <p class="text-center">No result found for query <strong>{{ request()->query('search') }}</strong></p>
                </td>
            </tr>
            @endforelse
        </tbody>
      </table>
    </div>

  </div>
  {!! $students->appends(['search' => request()->query('search')])->links() !!}
</div>
</div>
<!-- Table head options end -->

      </div>
    </div>
  </div>
@endsection
