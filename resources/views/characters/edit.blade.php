@extends('layouts.admin.default')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">Form Validation</h2>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index-2.html">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="#">Forms</a>
                  </li>
                  <li class="breadcrumb-item active">Form Validation
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
          <div class="form-group breadcrumb-right">
            <div class="dropdown">
              <a class="btn-icon btn btn-danger btn-round btn-sm dropdown-toggle" href="{{ route('karakter.index') }}">Back</a>
            </div>
          </div>
        </div>
      </div>
      <div class="content-body"><!-- Validation -->
<section class="bs-validation">
<div class="row">
  <!-- Bootstrap Validation -->
  <div class="col-md-12 col-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Bootstrap Validation</h4>
      </div>
      <div class="card-body">
        <form class="needs-validation" method="POST" action="{{ route('karakter.update', $character->id) }}">
            @csrf
            @method('PUT')
          <div class="form-group">
            <label class="form-label" for="basic-addon-name">Nomor Induk (NIS/NISN)</label>
            <select class="form-control" name="nomor_induk" id="select-country1" required>
                <option value="" disabled selected>Pilih Nomor Induk</option>
                <option value="{{ $character->student->id }}">{{ $character->student->nomor_induk }}</option>
              </select>
          </div>
          <div class="form-group">
            <label class="form-label" for="basic-default-email1">Category Karakter</label>
            <select class="form-control" name="karakter" id="select-country1" required>
                <option value="" disabled selected>Pilih Category Karakter</option>
                <option value="Religius">Religius</option>
                <option value="Disiplin">Disiplin</option>
                <option value="Tanggung Jawab">Tanggung Jawab</option>
                <option value="Mandiri">Mandiri</option>
                <option value="Literasi">Literasi</option>
              </select>
          </div>
          <div class="form-group">
            <label for="select-country1">Capaian</label>
            <input
              type="text"
              id="basic-default-password1"
              class="form-control"
              name="capaian"
              value="{{ $character->capaian }}"
              placeholder="Capaian"
              required
            />
          </div>
          <div class="form-group">
            <label for="dob-bootstrap-val">Deskripsi</label>
            <textarea type="text"
            id="dob-bootstrap-val"
            class="form-control"
            name="deskripsi"
            placeholder="Deskripsi"
            required cols="30" rows="10">{{ $character->deskripsi }}</textarea>
          </div>
          <div class="alert alert-danger mt-1 alert-validation-msg" role="alert">
            <div class="alert-body">
              <i data-feather="info" class="mr-50 align-middle"></i>
              <span><strong>Note:</strong> Harap mengisi format sesuai contoh yang ada di dalam kurung!</span>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- /Bootstrap Validation -->
</div>
</section>
<!-- /Validation -->

      </div>
    </div>
  </div>
@endsection
