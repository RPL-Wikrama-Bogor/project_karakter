@extends('layouts.admin.default')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">Form Validation</h2>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index-2.html">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="#">Forms</a>
                  </li>
                  <li class="breadcrumb-item active">Form Validation
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
          <div class="form-group breadcrumb-right">
            <div class="dropdown">
              <a class="btn-icon btn btn-danger btn-round btn-sm dropdown-toggle" href="{{ route('jurusan.index') }}">Back</a>
            </div>
          </div>
        </div>
      </div>
      <div class="content-body"><!-- Validation -->
<section class="bs-validation">
<div class="row">
  <!-- Bootstrap Validation -->
  <div class="col-md-12 col-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Bootstrap Validation</h4>
      </div>
      <div class="card-body">
        <form class="needs-validation" method="POST" action="{{ route('jurusan.store') }}">
            @csrf
          <div class="form-group">
            <label class="form-label" for="basic-addon-name">Jurusan (Rekayasa Perangkat Lunak)</label>

            <input
              type="text"
              id="basic-addon-name"
              class="form-control"
              placeholder="Tambah jurusan"
              name="jurusan"
              aria-describedby="basic-addon-name"
              required
            />
          </div>
          <div class="alert alert-danger mt-1 alert-validation-msg" role="alert">
            <div class="alert-body">
              <i data-feather="info" class="mr-50 align-middle"></i>
              <span><strong>Note:</strong> Harap mengisi format sesuai contoh yang ada di dalam kurung!</span>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- /Bootstrap Validation -->
</div>
</section>
<!-- /Validation -->

      </div>
    </div>
  </div>
@endsection
