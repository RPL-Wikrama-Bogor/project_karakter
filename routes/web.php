<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CharacterController;
use App\Http\Controllers\JurusanController;
use App\Http\Controllers\RombelController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('auth')->name('welcome');
Route::get('/profile', [UserController::class, 'profile'])->name('profile');
Route::post('/avatar-post', [UserController::class, 'update_profile'])->name('profile.post');
Route::post('/change-password-post', [UserController::class, 'change_password'])->name('password.post');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('karakter', CharacterController::class);
Route::resource('siswa', StudentController::class);
Route::resource('rombel', RombelController::class);
Route::resource('jurusan', JurusanController::class);
